import random
import datetime

today = datetime.date.today()
year = today.year

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


# what_age = random.randint(5, 45)
# which_birth_year = year - what_age


def is_adult(what_age):
    if what_age >= 18:
        return True
    else:
        return False


# def employee():
#     if random.randint(0, 1) % 2 == 0:
#         firstname = (random.choice(female_fnames))
#         lastname = (random.choice(surnames))
#         country = (random.choice(countries))
#         email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
#         age = what_age
#         adult = is_adult(what_age)
#         birth_year =which_birth_year
#
#     else:
#         firstname = (random.choice(male_fnames))
#         lastname = (random.choice(surnames))
#         country = (random.choice(countries))
#         email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
#         age = what_age
#         adult = is_adult(what_age)
#         birth_year = which_birth_year
#
#         return {
#             "firstname": firstname,
#             "lastname": lastname,
#             "country": country,
#             "email": email,
#             "age": age,
#             "adult": adult,
#             "birth_year": birth_year,
#         }

def employee_2():
    employee_2 = []

    for x in range(10):
        if x % 2 == 0:
            firstname = (random.choice(female_fnames))
            lastname = (random.choice(surnames))
            country = (random.choice(countries))
            email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
            age = random.randint(5, 45)
            adult = is_adult(age)
            birth_year = year - age

        else:
            firstname = (random.choice(male_fnames))
            lastname = (random.choice(surnames))
            country = (random.choice(countries))
            email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
            age = random.randint(5, 45)
            adult = is_adult(age)
            birth_year = year - age

            return (
                {
                    "firstname": firstname,
                    "lastname": lastname,
                    "country": country,
                    "email": email,
                    "age": age,
                    "adult": adult,
                    "birth_year": birth_year,

                })


def man_employee(firstname, lastname, country, email, age, adult, birth_year):
    firstname = (random.choice(male_fnames))
    lastname = (random.choice(surnames))
    country = (random.choice(countries))
    email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
    age = random.randint(5, 45)
    adult = is_adult(age)
    birth_year = year - age

    return {
        "firstname": firstname,
        "lastname": lastname,
        "country": country,
        "email": email,
        "age": age,
        "adult": adult,
        "birth_year": birth_year,
    }


def woman_employee(firstname, lastname, country, email, age, adult, birth_year):
    firstname = (random.choice(female_fnames))
    lastname = (random.choice(surnames))
    country = (random.choice(countries))
    email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
    age = random.randint(5, 45)
    adult = is_adult(age)
    birth_year = year - age

    return {
        "firstname": firstname,
        "lastname": lastname,
        "country": country,
        "email": email,
        "age": age,
        "adult": adult,
        "birth_year": birth_year,
    }


def list_of_women(entries_numer):
    list_of_women_employees = []
    for i in range(entries_numer):
        list_of_women_employees.append(woman_employee("A", "B", "Poland", "jakis@email.com", 23, True, 2001))
    return list_of_women_employees


def list_of_men(entries_numer):
    list_of_men_employees = []
    for i in range(entries_numer):
        list_of_men_employees.append(man_employee("Jon", "Jon", "Poland", "jakis@email.com", 23, True, 2001))
    return list_of_men_employees


def combined_list_of_employees():
    # list_of_employees = []
    # list_of_employees.append(list_of_women(5))
    # list_of_employees.append(list_of_men(5))
    # return list_of_employees
    for x in range(10):
        if x % 2 == 0:
            woman_employee()


def say_hi_to_everybody(people_list):
    for someone in people_list:
        print(f'Hi! {someone["firstname"]}')


if __name__ == '__main__':
    # print(man_employee("A", "B","A","b", 3 , 3,  200))
    # print(woman_employee("A", "B","A","b", 3 , 3,  200))
    # print(list_of_men(5))
    # print(list_of_men(5))
    # print(combined_list_of_employees())
    list = list_of_men(10)
    print(employee_2())
    # # print(say_hi_to_everybody())
    # list_of_people = combined_list_of_employees()
    print(say_hi_to_everybody(list))
