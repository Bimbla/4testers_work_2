import random
import datetime

today = datetime.date.today()
year = today.year

# Listy zawierające dane do losowania
female_names = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_names = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def is_adult(age):
    if age >= 18:
        return True
    else:
        return False


def man_employee():
    firstname = (random.choice(male_names))
    lastname = (random.choice(surnames))
    country = (random.choice(countries))
    email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
    age = random.randint(5, 45)
    adult = is_adult(age)
    birth_year = year - age

    return {
        "firstname": firstname,
        "lastname": lastname,
        "country": country,
        "email": email,
        "age": age,
        "adult": adult,
        "birth_year": birth_year,
    }


def woman_employee():
    firstname = (random.choice(female_names))
    lastname = (random.choice(surnames))
    country = (random.choice(countries))
    email = (str.lower(firstname)) + (str.lower(lastname)) + "@example.com"
    age = random.randint(5, 45)
    adult = is_adult(age)
    birth_year = year - age

    return {
        "firstname": firstname,
        "lastname": lastname,
        "country": country,
        "email": email,
        "age": age,
        "adult": adult,
        "birth_year": birth_year,
    }


def list_of_women():
    list_of_women_employees = []
    for i in range(5):
        list_of_women_employees.append(woman_employee())
    return list_of_women_employees


def list_of_men():
    list_of_men_employees = []
    for i in range(5):
        list_of_men_employees.append(man_employee())
    return list_of_men_employees


def combined_list_of_employees():
    list_of_employees = []
    for x in range(10):
        if x % 2 == 0:
            list_of_employees.append(woman_employee())
        else:
            list_of_employees.append(man_employee())
    return list_of_employees


def say_hi_to_everybody(people_list):
    for someone in people_list:
        print(
            f'Hi! I\'m {someone["firstname"]} {someone["lastname"]}. I come from {someone["country"]} and I was born in {someone["birth_year"]}.')


if __name__ == '__main__':
    # print(list_of_men())
    # print(list_of_women())
    print(combined_list_of_employees())
    name_list = combined_list_of_employees()
    print(say_hi_to_everybody(name_list))
