import random

list_of_names_men = ['Adam', 'Janusz', 'Pawel', "Nataniel", "Mateusz"]
list_of_names_women = ['Anna', 'Ewa', 'Agata', 'Jolanta', 'Maria', ]
list_of_domain = ['@example.com', '@papaya.com', '@world.com', '@global.com', '@business.com']
list_of_gender = [True, False]


def employee(email, seniority_years, female):
    if random.randint(0, 1) % 2 == 0:
        name = (random.choice(list_of_names_men))
        domain = (random.choice(list_of_domain))
        email = (name + domain)
        seniority_years = (random.randint(0, 30))
        # return [email, seniority_years, female]
        return {
            "emali": email,
            "seniority_years": seniority_years,
            "female": False,

        }
    else:

        name = (random.choice(list_of_names_women))
        domain = (random.choice(list_of_domain))
        email = (name + domain)
        seniority_years = (random.randint(0, 30))
        # return [email, seniority_years, female]
        return {
            "emali": email,
            "seniority_years": seniority_years,
            "female": True,

        }


def generate_list_of_employees(entries_numer):
    employees = []
    # print(entries_numer)
    for i in range(entries_numer):
        employees.append(employee("email", 0, True))
    return employees


def filter_list_by_seniority_years(employees, seniority_years):
    output = []
    for employee in employees:
        if employee["seniority_years"] > seniority_years:
            output.append(employee)
    return output


def filter_list_by_gender(employees, female=True):
    output = []
    for employee in employees:
        if employee["female"] == female:
            output.append(employee)
    return output


def filter_list_by_gender_men(employees, female=False):
    output = []
    for employee in employees:
        if employee["female"] == female:
            output.append(employee)
    return output


if __name__ == '__main__':
    # print(employee('john@example.com', 5, False))
    # print(employee('john@example.com', 5, False))
    # print(employee('john@example.com', 5, False))
    # print(employee('john@example.com', 5, False))

    # print(generate_list_of_employees(10))
    employees = generate_list_of_employees(10)
    print(employees)
    print(filter_list_by_seniority_years(employees,10))
    print(filter_list_by_gender_men(employees,False))