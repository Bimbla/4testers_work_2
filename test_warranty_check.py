from warranty_check import check_if_the_car_is_under_warranty


def test_check_if_the_car_is_under_warranty():
    assert check_if_the_car_is_under_warranty(2019, 4500) == True
def test_check_if_the_car_is_under_warranty():
    assert check_if_the_car_is_under_warranty(2024, 120000) == False
def test_check_if_the_car_is_under_warranty():
    assert check_if_the_car_is_under_warranty(1990, 4500) == False
def test_check_if_the_car_is_under_warranty():
    assert check_if_the_car_is_under_warranty(2024, 0) ==True


