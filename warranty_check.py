import datetime

today = datetime.date.today()
year = today.year


def check_if_the_car_is_under_warranty(year_of_production, mileage):
    age = year -year_of_production
    print(age)
    if age > 5 or mileage > 60000:
        return False
    else:
        return True


if __name__ == '__main__':
    print(check_if_the_car_is_under_warranty(2020, 5000))
    print(check_if_the_car_is_under_warranty(2000, 50000))
